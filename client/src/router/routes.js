import MainPage from 'layouts/MainPage';


const routes = [{
    path: '/',
    component: MainPage,
    children: []
  }
]

export default routes
